using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTypeComponent : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    protected ItemType m_ItemType;
    public ItemType Type
    {
        get
        {
            return m_ItemType;
        }
        set
        {
            m_ItemType = value;
        }
    }
    
}
