using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinMovmement : MonoBehaviour
{
    // Start is called before the first frame update
    
    [SerializeField] private float m_Angularspeed = 5.0f;

    [SerializeField] private Vector3 m_AxisOfRotation = new Vector3(1.0f, 0, 0);

    Transform m_ObjTransform;
    
    void Start()
    {
        m_ObjTransform = this.gameObject.GetComponent<Transform>();
        
    }

    // Update is called once per frame
    void Update()
    {
        m_ObjTransform.Rotate(m_AxisOfRotation, m_Angularspeed);
        
    }
}
