using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//ชนไอเทมต่างๆแล้วจะมีผล เช่น AddItem ไปที่ inventory หรือ case ItemType.POWERUP ให้ HealthPoint + 10
public class PlayerTriggerWithITC : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //Get components from item object
        //Get the ItemTypeComponent component from the triggered object
        ItemTypeComponent itc = other.GetComponent<ItemTypeComponent>();
        
        //Get components from the player
        //Inventory
        var inventory = GetComponent<Inventory>();
        
        //SimpleHealthPointComponent
        var simpleHP = GetComponent<SimpleHealthPointComponent>();

        //
        var moveSpeed = GetComponent<CapsulePlayerController>();


        if (itc != null)
        {
            switch (itc.Type)
            {
                case ItemType.COIN:
                    inventory.AddItem("COIN",1);
                    break;
                case ItemType.BIGCOIN:
                    inventory.AddItem("BIGCOIN",1);
                    break;
                case ItemType.POWERUP:
                    if(simpleHP != null)
                        simpleHP.HealthPoint = simpleHP.HealthPoint + 10;
                    break;
                case ItemType.POWERDOWN:
                    if(simpleHP != null)
                        simpleHP.HealthPoint = simpleHP.HealthPoint - 10;
                    break;
                case ItemType.SPEEDUP:
                    if (moveSpeed != null)
                        moveSpeed.Speed = moveSpeed.Speed + 10;
                    break;
                case ItemType.SPEEDDOWN:
                    if (moveSpeed != null)
                        moveSpeed.Speed = moveSpeed.Speed - 10;
                    break;
            }
        }
        Destroy(other.gameObject,0);
    }
    
}
