using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallImpulseLauncher : MonoBehaviour
{
    [SerializeField] protected float m_Mass;
    [SerializeField] protected float m_ForceMagnitude;
     [SerializeField] protected float m_LaunchInterval;
     
     [SerializeField] protected float m_LifeTime = 3;




    // Start is called before the first frame update
    void Start()
    {
        Invoke("LaunchBall", m_LaunchInterval);
    }

    private void LaunchBall()
    {
        GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
       go.transform.position = this.transform.position;
       Rigidbody rb = go.AddComponent<Rigidbody>();
      rb.mass = m_Mass;
      
      rb.AddForce(this.transform.forward* m_ForceMagnitude, ForceMode.Impulse);
      
      Destroy(go,m_LifeTime);
      
      Invoke("LaunchBall", m_LaunchInterval);
    }
}
