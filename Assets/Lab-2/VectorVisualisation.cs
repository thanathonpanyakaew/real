using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class VectorVisualisation : MonoBehaviour
{
    [SerializeField] protected float m_LineSize = 0.2f;
    [SerializeField] protected Material m_LineMaterial;
    
    [SerializeField] protected Rigidbody m_Rigidbody;
    protected LineRenderer m_LineRenderer;
    
    
    // Start is called before the first frame update
    protected virtual void Start()
    {
        if (m_Rigidbody == null)
        {
            m_Rigidbody = GetComponentInParent<Rigidbody>();
        }
        
        m_LineRenderer = gameObject.AddComponent<LineRenderer>();

        if (m_LineMaterial != null)
        {
            m_LineRenderer.material = m_LineMaterial;
        }
        
        m_LineRenderer.startWidth = m_LineSize;
        m_LineRenderer.endWidth = 0;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        
    }
}
