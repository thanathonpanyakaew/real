using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantForceVisualisation : VectorVisualisation
{
    
    [SerializeField] protected ConstantForce m_ConstantForce;
    
    
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        if (m_ConstantForce == null)
        {
            m_ConstantForce = GetComponentInParent<ConstantForce>();
        }
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        
        Vector3 forceStartPoint = m_Rigidbody.position ;

        if (m_ConstantForce.enabled)
        {
            forceStartPoint = m_Rigidbody.position - m_ConstantForce.force;
        }
        
        m_LineRenderer.SetPosition(0, forceStartPoint);
        m_LineRenderer.SetPosition(1, m_Rigidbody.position);
    }
}
