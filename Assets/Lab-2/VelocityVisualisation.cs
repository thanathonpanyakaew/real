using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class VelocityVisualisation : VectorVisualisation
{
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        
        Vector3 velocityEndPoint = m_Rigidbody.position + m_Rigidbody.velocity;
        
        m_LineRenderer.SetPosition(0, m_Rigidbody.position);
        m_LineRenderer.SetPosition(1, velocityEndPoint);
        
        
    }
}
